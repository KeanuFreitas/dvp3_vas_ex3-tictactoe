﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DVP3_VAS_EX3_TicTacToe
{
    public partial class Form1 : Form
    {
        // variables for the player turn and the count of how many times the buttons are being clicked
        bool playerTurn = true;
        int count = 0;

        private void winOrLose()
        {
            // member variables
            bool winner = false;
            string _winner = "";

            // Verticall conditional to check if there is a winner
            if ((oneBtn1.Text == twoBtn1.Text) && (twoBtn1.Text == threeBtn1.Text) && (!oneBtn1.Enabled))
            {
                winner = true;
            }
            else if ((oneBtn2.Text == twoBtn2.Text) && (twoBtn2.Text == threeBtn2.Text) && (!oneBtn2.Enabled))
            {
                winner = true;
            }
            else if ((oneBtn3.Text == twoBtn3.Text) && (twoBtn3.Text == threeBtn3.Text) && (!oneBtn3.Enabled))
            {
                winner = true;
            }

            // Horizontal conditional to check if there is a winner
            else if ((oneBtn1.Text == oneBtn2.Text) && (oneBtn2.Text == oneBtn3.Text) && (!oneBtn1.Enabled))
            {
                winner = true;
            }
            else if ((twoBtn1.Text == twoBtn2.Text) && (twoBtn2.Text == twoBtn3.Text) && (!twoBtn1.Enabled))
            {
                winner = true;
            }
            else if ((threeBtn1.Text == threeBtn2.Text) && (threeBtn2.Text == threeBtn3.Text) && (!threeBtn1.Enabled))
            {
                winner = true;
            }

            // Diagonal conditional to check if there is a winner
            else if ((oneBtn1.Text == twoBtn2.Text) && (twoBtn2.Text == threeBtn3.Text) && (!oneBtn1.Enabled))
            {
                winner = true;
            }
            else if ((oneBtn3.Text == twoBtn2.Text) && (twoBtn2.Text == threeBtn1.Text) && (!threeBtn1.Enabled))
            {
                winner = true;
            }

            // Depending on which radio button was chosen, this is if X was chosen
            if (xRadioBtn.Checked == true)
            {
                //Checking to see if there is a winner or a draw
                if (winner)
                {
                    if (playerTurn)
                    {
                        _winner = "O";
                    }
                    else
                    {
                        _winner = "X";
                    }

                    DialogResult result = MessageBox.Show(_winner + " is the winner!");
                    if (result == DialogResult.OK)
                    {
                        count = 0;
                        playerTurn = true;
                        oneBtn1.Text = "";
                        oneBtn1.Enabled = true;
                        oneBtn2.Text = "";
                        oneBtn2.Enabled = true;
                        oneBtn3.Text = "";
                        oneBtn3.Enabled = true;
                        twoBtn1.Text = "";
                        twoBtn1.Enabled = true;
                        twoBtn2.Text = "";
                        twoBtn2.Enabled = true;
                        twoBtn3.Text = "";
                        twoBtn3.Enabled = true;
                        threeBtn1.Text = "";
                        threeBtn1.Enabled = true;
                        threeBtn2.Text = "";
                        threeBtn2.Enabled = true;
                        threeBtn3.Text = "";
                        threeBtn3.Enabled = true;
                    }
                }

                // Checking the count to see if it reaches 9, then it is a draw
                if (count == 9)
                {
                    DialogResult result = MessageBox.Show("It was a draw!");
                    if (result == DialogResult.OK)
                    {
                        count = 0;
                        playerTurn = true;
                        oneBtn1.Text = "";
                        oneBtn1.Enabled = true;
                        oneBtn2.Text = "";
                        oneBtn2.Enabled = true;
                        oneBtn3.Text = "";
                        oneBtn3.Enabled = true;
                        twoBtn1.Text = "";
                        twoBtn1.Enabled = true;
                        twoBtn2.Text = "";
                        twoBtn2.Enabled = true;
                        twoBtn3.Text = "";
                        twoBtn3.Enabled = true;
                        threeBtn1.Text = "";
                        threeBtn1.Enabled = true;
                        threeBtn2.Text = "";
                        threeBtn2.Enabled = true;
                        threeBtn3.Text = "";
                        threeBtn3.Enabled = true;
                    }
                }
            }

            // if the O radio button was chosen
            else
            {
                //Checking to see if there is a winner or a draw
                if (winner)
                {
                    if (playerTurn)
                    {
                        _winner = "X";
                    }
                    else
                    {

                        _winner = "O";
                    }
                    DialogResult result = MessageBox.Show(_winner + " is the winner!");
                    if (result == DialogResult.OK)
                    {
                        count = 0;
                        playerTurn = true;
                        oneBtn1.Text = "";
                        oneBtn1.Enabled = true;
                        oneBtn2.Text = "";
                        oneBtn2.Enabled = true;
                        oneBtn3.Text = "";
                        oneBtn3.Enabled = true;
                        twoBtn1.Text = "";
                        twoBtn1.Enabled = true;
                        twoBtn2.Text = "";
                        twoBtn2.Enabled = true;
                        twoBtn3.Text = "";
                        twoBtn3.Enabled = true;
                        threeBtn1.Text = "";
                        threeBtn1.Enabled = true;
                        threeBtn2.Text = "";
                        threeBtn2.Enabled = true;
                        threeBtn3.Text = "";
                        threeBtn3.Enabled = true;
                    }
                }
                if (count == 9)
                {
                    DialogResult result = MessageBox.Show("It was a draw!");
                    if (result == DialogResult.OK)
                    {
                        count = 0;
                        playerTurn = true;
                        oneBtn1.Text = "";
                        oneBtn1.Enabled = true;
                        oneBtn2.Text = "";
                        oneBtn2.Enabled = true;
                        oneBtn3.Text = "";
                        oneBtn3.Enabled = true;
                        twoBtn1.Text = "";
                        twoBtn1.Enabled = true;
                        twoBtn2.Text = "";
                        twoBtn2.Enabled = true;
                        twoBtn3.Text = "";
                        twoBtn3.Enabled = true;
                        threeBtn1.Text = "";
                        threeBtn1.Enabled = true;
                        threeBtn2.Text = "";
                        threeBtn2.Enabled = true;
                        threeBtn3.Text = "";
                        threeBtn3.Enabled = true;
                    }
                }
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Exiting the application when this button is clicked
            Application.Exit();
        }

        private void click_Button(object sender, EventArgs e)
        {
            // on button clicks event handler, for each button do these functions below
            // this is if the X radio button was chosen
            if (xRadioBtn.Checked != false | oRadioBtn.Checked != false)
            {
                Button _button = (Button)sender;

                if (xRadioBtn.Checked == true)
                {
                    if (playerTurn)
                    {
                        _button.Text = "X";
                    }
                    else
                    {
                        _button.Text = "O";
                    }

                    playerTurn = !playerTurn;
                    _button.Enabled = false;
                    count++;
                    winOrLose();
                }

                // if the O radio button was chosen
                else
                {
                    if (playerTurn)
                    {
                        _button.Text = "O";
                    }
                    else
                    {
                        _button.Text = "X";
                    }

                    playerTurn = !playerTurn;
                    _button.Enabled = false;
                    count++;
                    winOrLose();
                }
            }

            // return nothing if neither of the radio buttons were chosen
            else
            {
                return;
            }
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Resetting all the values on the form
            count = 0;
            playerTurn = true;
            oneBtn1.Text = "";
            oneBtn1.Enabled = true;
            oneBtn2.Text = "";
            oneBtn2.Enabled = true;
            oneBtn3.Text = "";
            oneBtn3.Enabled = true;
            twoBtn1.Text = "";
            twoBtn1.Enabled = true;
            twoBtn2.Text = "";
            twoBtn2.Enabled = true;
            twoBtn3.Text = "";
            twoBtn3.Enabled = true;
            threeBtn1.Text = "";
            threeBtn1.Enabled = true;
            threeBtn2.Text = "";
            threeBtn2.Enabled = true;
            threeBtn3.Text = "";
            threeBtn3.Enabled = true;

        }
    }
}
