﻿namespace DVP3_VAS_EX3_TicTacToe
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.oneBtn1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oneBtn2 = new System.Windows.Forms.Button();
            this.oneBtn3 = new System.Windows.Forms.Button();
            this.twoBtn1 = new System.Windows.Forms.Button();
            this.twoBtn2 = new System.Windows.Forms.Button();
            this.twoBtn3 = new System.Windows.Forms.Button();
            this.threeBtn1 = new System.Windows.Forms.Button();
            this.threeBtn2 = new System.Windows.Forms.Button();
            this.threeBtn3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.grpBx = new System.Windows.Forms.GroupBox();
            this.oRadioBtn = new System.Windows.Forms.RadioButton();
            this.xRadioBtn = new System.Windows.Forms.RadioButton();
            this.menuStrip1.SuspendLayout();
            this.grpBx.SuspendLayout();
            this.SuspendLayout();
            // 
            // oneBtn1
            // 
            this.oneBtn1.BackColor = System.Drawing.SystemColors.Window;
            this.oneBtn1.Font = new System.Drawing.Font("Franklin Gothic Medium", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oneBtn1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.oneBtn1.Location = new System.Drawing.Point(30, 154);
            this.oneBtn1.Name = "oneBtn1";
            this.oneBtn1.Size = new System.Drawing.Size(123, 115);
            this.oneBtn1.TabIndex = 0;
            this.oneBtn1.TabStop = false;
            this.oneBtn1.UseVisualStyleBackColor = false;
            this.oneBtn1.Click += new System.EventHandler(this.click_Button);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(428, 40);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newGameToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newGameToolStripMenuItem
            // 
            this.newGameToolStripMenuItem.Name = "newGameToolStripMenuItem";
            this.newGameToolStripMenuItem.Size = new System.Drawing.Size(232, 38);
            this.newGameToolStripMenuItem.Text = "New Game";
            this.newGameToolStripMenuItem.Click += new System.EventHandler(this.newGameToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(232, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // oneBtn2
            // 
            this.oneBtn2.BackColor = System.Drawing.SystemColors.Window;
            this.oneBtn2.Font = new System.Drawing.Font("Franklin Gothic Medium", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oneBtn2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.oneBtn2.Location = new System.Drawing.Point(159, 154);
            this.oneBtn2.Name = "oneBtn2";
            this.oneBtn2.Size = new System.Drawing.Size(123, 115);
            this.oneBtn2.TabIndex = 0;
            this.oneBtn2.TabStop = false;
            this.oneBtn2.UseVisualStyleBackColor = false;
            this.oneBtn2.Click += new System.EventHandler(this.click_Button);
            // 
            // oneBtn3
            // 
            this.oneBtn3.BackColor = System.Drawing.SystemColors.Window;
            this.oneBtn3.Font = new System.Drawing.Font("Franklin Gothic Medium", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oneBtn3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.oneBtn3.Location = new System.Drawing.Point(288, 154);
            this.oneBtn3.Name = "oneBtn3";
            this.oneBtn3.Size = new System.Drawing.Size(123, 115);
            this.oneBtn3.TabIndex = 0;
            this.oneBtn3.TabStop = false;
            this.oneBtn3.UseVisualStyleBackColor = false;
            this.oneBtn3.Click += new System.EventHandler(this.click_Button);
            // 
            // twoBtn1
            // 
            this.twoBtn1.BackColor = System.Drawing.SystemColors.Window;
            this.twoBtn1.Font = new System.Drawing.Font("Franklin Gothic Medium", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.twoBtn1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.twoBtn1.Location = new System.Drawing.Point(30, 275);
            this.twoBtn1.Name = "twoBtn1";
            this.twoBtn1.Size = new System.Drawing.Size(123, 115);
            this.twoBtn1.TabIndex = 0;
            this.twoBtn1.TabStop = false;
            this.twoBtn1.UseVisualStyleBackColor = false;
            this.twoBtn1.Click += new System.EventHandler(this.click_Button);
            // 
            // twoBtn2
            // 
            this.twoBtn2.BackColor = System.Drawing.SystemColors.Window;
            this.twoBtn2.Font = new System.Drawing.Font("Franklin Gothic Medium", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.twoBtn2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.twoBtn2.Location = new System.Drawing.Point(159, 275);
            this.twoBtn2.Name = "twoBtn2";
            this.twoBtn2.Size = new System.Drawing.Size(123, 115);
            this.twoBtn2.TabIndex = 0;
            this.twoBtn2.TabStop = false;
            this.twoBtn2.UseVisualStyleBackColor = false;
            this.twoBtn2.Click += new System.EventHandler(this.click_Button);
            // 
            // twoBtn3
            // 
            this.twoBtn3.BackColor = System.Drawing.SystemColors.Window;
            this.twoBtn3.Font = new System.Drawing.Font("Franklin Gothic Medium", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.twoBtn3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.twoBtn3.Location = new System.Drawing.Point(288, 275);
            this.twoBtn3.Name = "twoBtn3";
            this.twoBtn3.Size = new System.Drawing.Size(123, 115);
            this.twoBtn3.TabIndex = 0;
            this.twoBtn3.TabStop = false;
            this.twoBtn3.UseVisualStyleBackColor = false;
            this.twoBtn3.Click += new System.EventHandler(this.click_Button);
            // 
            // threeBtn1
            // 
            this.threeBtn1.BackColor = System.Drawing.SystemColors.Window;
            this.threeBtn1.Font = new System.Drawing.Font("Franklin Gothic Medium", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeBtn1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.threeBtn1.Location = new System.Drawing.Point(30, 396);
            this.threeBtn1.Name = "threeBtn1";
            this.threeBtn1.Size = new System.Drawing.Size(123, 115);
            this.threeBtn1.TabIndex = 0;
            this.threeBtn1.TabStop = false;
            this.threeBtn1.UseVisualStyleBackColor = false;
            this.threeBtn1.Click += new System.EventHandler(this.click_Button);
            // 
            // threeBtn2
            // 
            this.threeBtn2.BackColor = System.Drawing.SystemColors.Window;
            this.threeBtn2.Font = new System.Drawing.Font("Franklin Gothic Medium", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeBtn2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.threeBtn2.Location = new System.Drawing.Point(159, 396);
            this.threeBtn2.Name = "threeBtn2";
            this.threeBtn2.Size = new System.Drawing.Size(123, 115);
            this.threeBtn2.TabIndex = 0;
            this.threeBtn2.TabStop = false;
            this.threeBtn2.UseVisualStyleBackColor = false;
            this.threeBtn2.Click += new System.EventHandler(this.click_Button);
            // 
            // threeBtn3
            // 
            this.threeBtn3.BackColor = System.Drawing.SystemColors.Window;
            this.threeBtn3.Font = new System.Drawing.Font("Franklin Gothic Medium", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeBtn3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.threeBtn3.Location = new System.Drawing.Point(288, 396);
            this.threeBtn3.Name = "threeBtn3";
            this.threeBtn3.Size = new System.Drawing.Size(123, 115);
            this.threeBtn3.TabIndex = 0;
            this.threeBtn3.TabStop = false;
            this.threeBtn3.UseVisualStyleBackColor = false;
            this.threeBtn3.Click += new System.EventHandler(this.click_Button);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 19.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(46, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(351, 94);
            this.label1.TabIndex = 10;
            this.label1.Text = "Tic-tac-toe";
            // 
            // grpBx
            // 
            this.grpBx.Controls.Add(this.oRadioBtn);
            this.grpBx.Controls.Add(this.xRadioBtn);
            this.grpBx.Location = new System.Drawing.Point(30, 534);
            this.grpBx.Name = "grpBx";
            this.grpBx.Size = new System.Drawing.Size(192, 154);
            this.grpBx.TabIndex = 11;
            this.grpBx.TabStop = false;
            this.grpBx.Text = "Player choice";
            // 
            // oRadioBtn
            // 
            this.oRadioBtn.AutoSize = true;
            this.oRadioBtn.Location = new System.Drawing.Point(17, 99);
            this.oRadioBtn.Name = "oRadioBtn";
            this.oRadioBtn.Size = new System.Drawing.Size(126, 29);
            this.oRadioBtn.TabIndex = 1;
            this.oRadioBtn.TabStop = true;
            this.oRadioBtn.Text = "Player O";
            this.oRadioBtn.UseVisualStyleBackColor = true;
            // 
            // xRadioBtn
            // 
            this.xRadioBtn.AutoSize = true;
            this.xRadioBtn.Location = new System.Drawing.Point(17, 49);
            this.xRadioBtn.Name = "xRadioBtn";
            this.xRadioBtn.Size = new System.Drawing.Size(124, 29);
            this.xRadioBtn.TabIndex = 0;
            this.xRadioBtn.TabStop = true;
            this.xRadioBtn.Text = "Player X";
            this.xRadioBtn.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 725);
            this.Controls.Add(this.grpBx);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.threeBtn3);
            this.Controls.Add(this.threeBtn2);
            this.Controls.Add(this.threeBtn1);
            this.Controls.Add(this.twoBtn3);
            this.Controls.Add(this.twoBtn2);
            this.Controls.Add(this.twoBtn1);
            this.Controls.Add(this.oneBtn3);
            this.Controls.Add(this.oneBtn2);
            this.Controls.Add(this.oneBtn1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grpBx.ResumeLayout(false);
            this.grpBx.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button oneBtn1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newGameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button oneBtn2;
        private System.Windows.Forms.Button oneBtn3;
        private System.Windows.Forms.Button twoBtn1;
        private System.Windows.Forms.Button twoBtn2;
        private System.Windows.Forms.Button twoBtn3;
        private System.Windows.Forms.Button threeBtn1;
        private System.Windows.Forms.Button threeBtn2;
        private System.Windows.Forms.Button threeBtn3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpBx;
        private System.Windows.Forms.RadioButton oRadioBtn;
        private System.Windows.Forms.RadioButton xRadioBtn;
    }
}

